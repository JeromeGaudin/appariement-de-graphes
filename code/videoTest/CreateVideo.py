import cv2
import os
import numpy as np
import re # regex

from src.rectangle import Rectangle

def createVideo(listRectStart, listRectEnd, videoSize, nbPic, nameDir):
    """
    Create video with this file det. Video is composed of color rectangle passed in argument
    
    Args:
        listRectStart: position of rectangle at start (you need to create Rectangle with attribute a, b and color) 
        listRectEnd: position of rectangle at end (you need to create Rectangle with attribute a, b)
        videoSize: tuple(height, width) dimension of one picture 
        nbPic: number of picture
        nameDir: string of path where directory will be create
    """
    
    if len(listRectStart) != len(listRectEnd):
        print("createVideo error: both lists must have the same size")
        return
    
    move_xA = []
    move_yA = []
    move_xB = []
    move_yB = []
    
    color = []
    
    for i in range(0, len(listRectStart)):
        
        ad = listRectStart[i].origin
        bd = listRectStart[i].computeOriginB()

        af = listRectEnd[i].origin
        bf = listRectEnd[i].computeOriginB()
        
        move_xA.append(np.linspace(ad[0], af[0], nbPic, dtype=int))
        move_yA.append(np.linspace(ad[1], af[1], nbPic, dtype=int))
        move_xB.append(np.linspace(bd[0], bf[0], nbPic, dtype=int))
        move_yB.append(np.linspace(bd[1], bf[1], nbPic, dtype=int))
        
        color.append(listRectStart[i].color)
    
    
    os.mkdir(nameDir)
    os.mkdir(nameDir + "det/")
    os.mkdir(nameDir + "img1/")
    
    fileDet = open(nameDir + "det/det.txt", 'w')
    
    #foreach picture
    for i in range(0, nbPic):
        
        # create a matrice like a picture
        image = np.zeros((videoSize[0], videoSize[1], 3), np.uint8)
        
        for j in range(0, len(listRectStart)):
            
            # draw a rectangle
            image[move_yA[j][i] : move_yB[j][i], move_xA[j][i] : move_xB[j][i]] = color[j]# (B, G, R)
        
            # if size not equal to 0 and if the rectangle does not extend beyond the image
            if (move_xA[j][i] != move_xB[j][i] and move_yA[j][i] != move_yB[j][i]) and (move_xB[j][i] <= videoSize[1] and move_yB[j][i] <= videoSize[0]):
                    #writing in det file
                    fileDet.write(str(i+1)+","+"-1,"+str(move_xA[j][i])+","+str(move_yA[j][i])+","+str(move_xB[j][i]-move_xA[j][i])+","+str(move_yB[j][i]-move_yA[j][i])+"\n")
    
        if i < 9:
            number = '000' + str(i+1)
        elif i<99:
            number = '00' + str(i+1) 
        elif i<999:
            number = '0' + str(i+1)
        
        # write picture on file
        cv2.imwrite(nameDir + "img1/" + number + ".png", image)
        
    # shuffle line in file det
    shuffleFileDet(nameDir + "det/det.txt")
    
    fileDet.close()    
    print("Vidéo créée")
    
    
def funcSortDetLine(string):
    """
    return the first number of a string
    Args:
        string the string
        
    Return:
        int the first number in string
    """
    res = re.search(r'\d+', string)
    res = int(res[0])
    return res
    
def shuffleFileDet(fileName):
    """
    Shuffle all line in detection file. But keep order of frame

    Args:
        fileName file name of detection file
    """
    lines = None
    # read all line of fileName
    with open(fileName, 'r') as f:
        lines = f.readlines()
    f.close()
    
    # shuffle all lines
    np.random.shuffle(lines)
    
    # sort just with frame
    lines.sort(key=funcSortDetLine)
    
    # overwrite the file with sorted lines
    fileDet = open(fileName, 'w')
    for l in lines:
        fileDet.write(l);

    fileDet.close() 
