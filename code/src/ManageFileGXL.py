import xml.etree.ElementTree as etree
import networkx as nx
import os

from src.rectangle import Rectangle


def writeFileGXL(graph, nameFile):
    """
    write a graph in a file in the format XML
    
    Args:
        graph: the networkX graph
        nameFile: path file where the file will be saved
    """
    
    file = open(nameFile, 'wb')

    g = etree.Element("graph")
    tree = etree.ElementTree(g)

    for i in range (0, nx.number_of_nodes(graph)):
        node = etree.SubElement(g, "node")
        attr = etree.SubElement(node, "attr")
        attr.set("name", "rectangle")

        n = nx.get_node_attributes(graph, "rectangle")[i]
        node.set("id", str(n.id))
        
        # origin
        origin = etree.SubElement(attr, "origin")
        x = etree.SubElement(origin, "x")
        x.text = str(n.origin[0])
        y = etree.SubElement(origin, "y")
        y.text = str(n.origin[1])
        
        # coordinates
        coordinates = etree.SubElement(attr, "coordinates")
        x = etree.SubElement(coordinates, "x")
        x.text = str(n.coordinates[0])
        y = etree.SubElement(coordinates, "y")
        y.text = str(n.coordinates[1])

        # dimension
        dimension = etree.SubElement(attr, "dimension")
        h = etree.SubElement(dimension, "h")
        h.text = str(n.dimension[0])
        w = etree.SubElement(dimension, "w")
        w.text = str(n.dimension[1])

        # avgColor
        avgColor = etree.SubElement(attr, "avgColor")
        red = etree.SubElement(avgColor, "red")
        red.text = str(n.avgColor[0])
        green = etree.SubElement(avgColor, "green")
        green.text = str(n.avgColor[1])
        blue = etree.SubElement(avgColor, "blue")
        blue.text = str(n.avgColor[2])

    # creation of edges
    edges = list(graph.edges.data('distance'))
    for i in range (0, nx.number_of_edges(graph)):
        edgeTree = etree.SubElement(g,"edge")
        edgeTree.set("from", str(edges[i][0]))
        edgeTree.set("to", str(edges[i][1]))
        attr = etree.SubElement(edgeTree, "attr")
        attr.set("name", "distance")
        attr.text = str(edges[i][2])
    

    tree.write(nameFile, encoding='unicode')
    file.close()

def readFileGXL(nameFile):
    """
    read a graph in a file in the format XML
    
    Args:
        nameFile: path file where the graph is saved
        
    Return:
        A networkX graph
    """
    tree = etree.parse(nameFile)
    root = tree.getroot()
    g = nx.Graph() # networkX graph to return
    countNode = 0
    
    # for each element on tree
    for child in root:
        if child.tag == "node":
            treeNode = child
            rect = Rectangle() # create an empty rectangle 
            rect.id = int(treeNode.attrib['id'])
            treeAttr = list(treeNode)[0] # recovers the attr tag
            for cat in treeAttr:
                if cat.tag == 'origin':
                    x = y = 0
                    for elem in cat:
                        if elem.tag == 'x':
                            x = int(elem.text)
                        if elem.tag == 'y':
                            y = int(elem.text)
                    rect.origin = (x, y)
                if cat.tag == 'coordinates':
                    x = y = 0
                    for elem in cat:
                        if elem.tag == 'x':
                            x = int(elem.text)
                        if elem.tag == 'y':
                            y = int(elem.text)
                    rect.coordinates = (x, y)
                if cat.tag == 'dimension':
                    h = w = 0
                    for elem in cat:
                        if elem.tag == 'h':
                            h = int(elem.text)
                        if elem.tag == 'w':
                            w = int(elem.text)
                    rect.dimension = (h, w)
                if cat.tag == 'avgColor':
                    red = green = blue = 0
                    for elem in cat:
                        if elem.tag == 'red':
                            red = float(elem.text)
                        if elem.tag == 'green':
                            green = float(elem.text)
                        if elem.tag == 'blue':
                            blue = float(elem.text)
                    rect.avgColor = (red, green, blue)
            g.add_node(countNode, rectangle=rect)
            countNode = countNode + 1
    
    
    # for each tag edge
    for edge in tree.findall(".//edge"):
        
        start = int(edge.get('from'))
        end = int(edge.get('to'))
        edgeAttr = list(edge)[0]
        g.add_edge(start,end, distance=float(edgeAttr.text))
    
    return g

def readAllFileGXL(nameDir):
    """
    read all graphs in a directory in the format XML
    
    Args:
        nameDir: directory where all graphs are saved
        
    Return:
        A list of networkX graph
    """
    nameFileGraph = os.listdir(nameDir)
    nameFileGraph = sorted(nameFileGraph,key=lambda x: int(os.path.splitext(x)[0]))# to sort in numeric order
    
    n = len(nameFileGraph)
    listGraph = []
    for i in range(0, n):
        nameGraph = nameDir + nameFileGraph[i]
        listGraph.append(readFileGXL(nameGraph))
    return listGraph  
        
    