
def extractRect(nameFile):
    """
    Extract all rectangle of detection file (.det)
    
    Args:
        nameFile: detection file path 
        
    Return:
        list: return list of array. Each array contain coordination point at the top left and at the bottom right : [(x1,y1),(x2,y2)]
    """
    listRect = []
    file = open(nameFile, 'r')
    
    line = file.readline()
    idCurrent = -1
    while line != '':
        e = line.split(",")
        frame_id = int(e[0]) -1
        
        if idCurrent < frame_id:
            listRect.append([])
            idCurrent = frame_id

        x = int(float(e[2]))
        y = int(float(e[3]))
        lx = int(float(e[4]))
        ly = int(float(e[5]))
        rect = [(x,y),(x+lx,y+ly)]
        listRect[frame_id].append(rect)

        line = file.readline()

    return listRect
