import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import os

from src.rectangle import Rectangle
from src.ManageFileGXL import writeFileGXL
from src.match import matching, permuteNodeGraph

# constant
from src import MAX_DISTANCE

def generateAllGraph(listFrame, listRect, dirPath):
    """
    Generate all graphs with the listFrame and listRect and save it in nameFile
    
    Args:
        listFrame: list of frame
        listRect: list of (list of object Rectangle per frame)
        dirPath: folder path where graphs will be save  
    """
    # if nameFile don't exist create it 
    if not os.path.isdir(dirPath):
        os.makedirs(dirPath)
    
    nBFrame = len(listFrame)
    
    oldGraph = None # graph has the index i-1 
    for i in range (0, nBFrame):
        # create one graph
        graph = generateOneGraph(listFrame[i], listRect[i])
        
        
        if __debug__ and i == 0:
            print("graph 1")
            rects = nx.get_node_attributes(graph, "rectangle")
            for rect in range(0, len(rects)):
                print(rects[rect].avgColor)
        
        # matching
        if (oldGraph is not None):
            print("match "+str(i)+"/"+str(nBFrame-1))
            tuples = matching(oldGraph, graph)
            permuteNodeGraph(oldGraph, graph, tuples)
            oldGraph = graph
        
        # write this graph on a file
        nameFile = dirPath + str(i+1) + '.gxl'
        writeFileGXL(graph, nameFile)
        
        oldGraph = graph
        
        

def generateOneGraph(frame, listRectForOneFrame):
    """
    Generate one graph with frame and listRectForOneFrame and save
    
    Args:
        listFrame: list of frame
        listRectForOneFrame: list of object Rectangle in frame
    
    Return:
        return a networkX graph where a node corresponding a rectangle with this attributes
    """
    g = nx.Graph()
    for i in range( 0, len(listRectForOneFrame)):
        # creation of a node
        a = listRectForOneFrame[i][0]
        b = listRectForOneFrame[i][1]
        tmpRect = Rectangle(a, b, frame)
        g.add_node(i, rectangle=tmpRect)

        # edge creation
        a = np.array(tmpRect.coordinates)

        # add attributes to node
        listRect = nx.get_node_attributes(g, 'rectangle')
        
        for j in range(0,i):   
            b = np.array(listRect[j].coordinates)
            d = np.linalg.norm(b-a)            
            # create an edge if distance between node are equal or less than MAX_DISTANCE
            if (d <= MAX_DISTANCE):
                g.add_edge(i,j,distance=d)
    return g



def showGraphRectangle(graph):
    """
    Show one graph. Each node must contain an attribute rectangle corresponding to the Rectangle object  
    
    Args:
        graph: The graph to show
    """    
    rect = nx.get_node_attributes(graph,'rectangle')
    # creation of dictionaries
    nodeLabels = {}
    nodePos = {}
     
    # loop for put each attribute in dictionary
    for i in range(0, len(rect)):
        nodeLabels[i] = rect[i].id
        (x, y) = rect[i].coordinates
        nodePos[i] = []
        nodePos[i].append(x)
        nodePos[i].append(-y)
    
    nx.draw(graph, nodePos)
    nx.draw_networkx_labels(graph, pos=nodePos, labels=nodeLabels)
    plt.show()
    
    
def listGraphToListRect(listGraph):
    """
    Return a list of list of objects Rectangle
    
    Args:
        listGraph: the list of Graphs
        
    Return:
        A list of list of objects Rectangle
    """
    listRect = []
    nbGraph = len(listGraph) 
    for i in range(0,nbGraph):
        listRect.append(nx.get_node_attributes(listGraph[i], "rectangle")) 
    return listRect
        
