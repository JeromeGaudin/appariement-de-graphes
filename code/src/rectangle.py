import numpy as np

class Rectangle:
    """
        This class represent a detected object on a frame. 
    """

    def computeCoordinates(self, a, b):
        """
        Compute the coordinate of the point at the bottom in the middle of the rectangle
        
        Args: 
            a: tuple (x,y) corresponding to the point at the top left of the rectangle
            b: tuple (x,y) corresponding to the point at the bottom right of the rectangle
        """
        x1 = a[0]
        (x2, y2) = b
        x3 = int((x2+x1)/2)
        y3 = y2
        return (x3, y3)

        
    def computeDimension (self, a, b):
        """
        Compute dimensions of the rectangle: height and width 
        
        Args: 
            a: tuple (x,y) corresponding to the point at the top left of the rectangle
            b: tuple (x,y) corresponding to the point at the bottom right of the rectangle
            
        Return:
            A tuple (c, d) c corresponding to the width and d to the height
        """
        (x1, y1) = a
        (x2, y2) = b
        return (x2-x1, y2-y1)

    
    def computeAvgColor(self, a, b, frame):
        """
        compute the average of blue, green and red
        
        Args:
            a: tuple (x,y) corresponding to the point at the top left of the rectangle
            b: tuple (x,y) corresponding to the point at the bottom right of the rectangle
            frame: matrix representing a frame. It contain pixel color in sequence (Blue Green Red)  
            
        Return:
            a tuple(R,G,B) R for average of the reds, G for average of the greens and B for average of the blues
        """
        (x1, y1) = a
        (x2, y2) = b
        
        # if there don't have a frame, he can't compute color on frame
        if (frame is None):
            return (None, None, None)
        
        frameRect = frame[y1: y2, x1: x2]
        
        
        frame_array = np.array(frameRect)
        
        Red = np.average(frame_array[:,:,2])
        Green = np.average(frame_array[:,:,1])
        Blue = np.average(frame_array[:,:,0])
        return (Red, Green, Blue)
    
    def computeOriginB(self):
        """
        Compute the coordinate of the point at the bottom right of the rectangle with origin and dimension
          
        Return:
            a tuple(x, y)of the point at the bottom right of the rectangle
        """
        x2 = self.origin[0] + self.dimension[0]
        y2 = self.origin[1] + self.dimension[1]
        return (x2,y2)
    
    def computeCenter(self):
        """
        Compute the coordinate of the point on the center of the rectangle
        
        Return:
            a tuple(x, y) of the point on the center of the rectangle
        """
        y =  int(self.coordinates[1] - self.dimension[1] / 2)
        return (self.coordinates[0], y)


    def __init__(self, a=(0,0), b=(0,0), frame=None, color=(255,255,255)):
        """
            Create a Rectangle object with right attributes
            
            Args:
                a: tuple (x,y) corresponding to the point at the top left of the rectangle
                b: tuple (x,y) corresponding to the point at the bottom right of the rectangle
                frame: matrix representing a frame. It contain pixel color in sequence (Blue Green Red)
        """
        self.id = -1              # give an in to this object
        self.coordinates = self.computeCoordinates(a,b)
        self.dimension = self.computeDimension(a,b)
        self.avgColor = self.computeAvgColor(a, b, frame)
        self.color = color
        self.origin = a
        