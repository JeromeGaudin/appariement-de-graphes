"""
The src module
"""
import random

nextObjectId = 0

random.Random() # initialization for file video.py

# parameters from graph.py

# MAX_DISTANCE is maximal distance until which two node are connect
MAX_DISTANCE = 110


# parameters from match.py
limit1 = 50
limit2 = 50
limit3 = 50

# parameters from video.py
FRAME_RATE = 180

EDGE_COLOR = (255, 255, 255)
EDGE_THICKNESS = 2

NODE_COLOR = (255,255,255)
NODE_RADIUS = 4

DETECTION_LINE_THICKNESS = 2
