import os
import cv2
import random
import copy
import networkx as nx

# constants 
from src import NODE_RADIUS, NODE_COLOR, DETECTION_LINE_THICKNESS, EDGE_COLOR, EDGE_THICKNESS, FRAME_RATE



def extractFrame(nameVideo):
    """
    Read all images in a folder save in a list of matrix 
    
    Args:
        nameVideo: path of the folder that contains the video as a sequence of images   
        
    Return:
        list of frame. Each frame is represented by a matrix corresponding to one image. 
                All images contain pixels colored in sequence (Blue Green Red)
    """
    namePictures = os.listdir(nameVideo)
    namePictures.sort()
    n = len(namePictures)
    listFrame = []
    for i in range(0, n):
        print("read picture " + str(i+1) + "/" + str(n))
        namePicture = nameVideo + namePictures[i]
        listFrame.append(cv2.imread(namePicture))
    return listFrame


def drawDetection(frame, pointA, pointB, nbColor):
    """
    Return the frame with a rectangle around the detected object
    
    Args:
        frame: the frame
        pointA: tuple (x,y) corresponding to the point at the top left of the rectangle
        pointB: tuple (x,y) corresponding to the point at the bottom right of the rectangle
        nbColor: id for determine color
            
        Return:
            frame with the rectangle around the detected object
    """
    random.seed(nbColor)
    color = (int(random.uniform(0,256)), int(random.uniform(0,256)), int(random.uniform(0,256)))
    cv2.rectangle(frame, pointA, pointB, color, DETECTION_LINE_THICKNESS)
    return frame

def drawNode(frame, rectangleObject):
    """
    Return frame with a rectangle around the detected object
    
    Args:
        frame: the frame
        rectangleObject: a rectangle object
            
        Return:
            frame with the rectangle around the detected object
    """
    point = rectangleObject.computeCenter()
    cv2.circle(frame, point, NODE_RADIUS, NODE_COLOR, -1) # -1 = fill cercle
    return frame

def showRect(listFrame, listRect):
    """
    Show images with rectangles
    
    Args:
        listFrame: list of frame
        listRect: list of two points which represent a rectangle
    """
    listF = copy.deepcopy(listFrame) # copy list for don't change Frames
    
    for i in range(0,len(listF)):
        for j in range(0, len(listRect[i])):            
           drawDetection(listF[i], listRect[i][j][0], listRect[i][j][1], j)
    for i in range(0,len(listF)):
        cv2.imshow('myImage', listF[i])
        cv2.waitKey(FRAME_RATE)
        
        
def showObjectRect(listFrame, listObjectRect):
    """
    Show images with rectangles
    
    Args:
        listFrame: list of frame
        listObjectRect: list of object Rectangle
    """
    listF = copy.deepcopy(listFrame) # copy list for don't change Frames
    
    for i in range(len(listObjectRect)):
        for j in range(len(listObjectRect[i])):
            drawDetection(listF[i], listObjectRect[i][j].origin, listObjectRect[i][j].computeOriginB(), listObjectRect[i][j].id)
    for i in range(len(listF)):
        cv2.imshow('myImage', listF[i])
        cv2.waitKey(FRAME_RATE)
        
        
def showObjectRectWithGraph(listFrame, listGraph):
    """
    Show images with rectangles and over the graph with cricle
    
    Args:
        listFrame: list of frame
        listGraph: list of graph
    """
    listF = copy.deepcopy(listFrame) # copy list for don't change Frames
    
    for i in range(len(listGraph)):
        listRect = nx.get_node_attributes(listGraph[i], "rectangle")
        for j in range(len(listRect)):
            drawDetection(listF[i], listRect[j].origin, listRect[j].computeOriginB(), listRect[j].id)
        
        # draw edges of graph
        for edge in listGraph[i].edges:
            pointA = listRect[edge[0]].computeCenter()
            pointB = listRect[edge[1]].computeCenter()
            cv2.line(listF[i], pointA, pointB, EDGE_COLOR, EDGE_THICKNESS)
            
        # other loop for draw node graph over
        for j in range(len(listRect)):
            drawNode(listF[i], listRect[j])
    
    # display all frames
    for i in range(len(listF)):
        cv2.imshow('myImage', listF[i])
        cv2.waitKey(FRAME_RATE)
        