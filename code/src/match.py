import networkx as nx
import numpy as np

from src import limit1, limit2, limit3, nextObjectId

def compareNodes(a, b):
    """
    Compare two node a and b
    
    Args:
        a: node on a graph, it must contain a rectangle object
        b: node on a graph, it must contain a rectangle object
        
    Return:
        boolean return True if two nodes match, False else. 
    """
    
    rectA = a['rectangle']
    rectB = b['rectangle']
    
    # compare coordinates    
    p1 = np.array((rectA.coordinates[0], rectA.coordinates[1]))
    p2 = np.array((rectB.coordinates[0], rectB.coordinates[1]))

    d = np.linalg.norm(p2 - p1)
    if d > limit1:
        return False
    
    # compare dimensions
    dimA = rectA.dimension
    dimB = rectB.dimension
    
    d = dimA[0]*dimA[1] - dimB[0]*dimB[1]
    if d<0:
        d=-d
        
    if d > limit2:
        return False
    
    # compare color
    colorA = rectA.avgColor
    colorB = rectB.avgColor
    c1 = colorA[0] - colorB[0]
    c2 = colorA[1] - colorB[1]
    c3 = colorA[2] - colorB[2]
    if abs(c1) > limit3:
        return False
    if abs(c2) > limit3:
        return False
    if abs(c3) > limit3:
        return False
    
    return True
     
    
def matching(g1, g2):
    """
    Try to match each node between this two graphs
    
    Args: 
        g1: first graph
        g2: second graph
    
    Return:
        tuples of node position corresponding to the first graph with the second
    """
    return nx.optimal_edit_paths(g1, g2, node_match=compareNodes)

def permuteNodeGraph(oldGraph, graph, tuples):
    """
    permute id of node in the graph in parameter
    Args:
        oldGraph: graph with id in node 
        graph: nodes of graph will take id of oldGraph
        tuples: permutation
    """
    
    if __debug__:
        print(tuples)
        
    nbTuple = len(tuples[0])

    # select the last tuple for permutation
    tupleNodes = tuples[0][nbTuple -1][0]
    
    
    if __debug__:
        rects = nx.get_node_attributes(oldGraph, "rectangle")
        print("previous graph")
        for j in range(0, len(rects)):
            print(str(rects[j].id) + " " + str(rects[j].avgColor))
        print("--") 
                

    if __debug__:
        rects = nx.get_node_attributes(graph, "rectangle")
        print("Selected")
        print(tupleNodes)
    
    
    for i in range(len(tupleNodes)):
    
        permA = tupleNodes[i][0]
        permB = tupleNodes[i][1]
        if permB is not None:
            if permA is not None:
                # permutation
                if __debug__:
                    print("pos " + str(permB)+" " + str(graph.node[permB]['rectangle'].avgColor) + " <- pos " + str(permA) +" " + str(oldGraph.node[permA]['rectangle'].avgColor))
                    
                graph.node[permB]['rectangle'].id = oldGraph.node[permA]['rectangle'].id
                
            # if rectangle are id -1 then attribute a new id
            if graph.node[permB]['rectangle'].id == -1:
                global nextObjectId
                graph.node[permB]['rectangle'].id = nextObjectId
                nextObjectId += 1


    if __debug__:
        rects = nx.get_node_attributes(graph, "rectangle")   
        for j in range(0, len(rects)):
            print(str(rects[j].id) + " " + str(rects[j].avgColor))
        print("fin")
       
        