#-*- coding: utf-8 -*-
#!/usr/bin/env python

from src.graph import generateAllGraph, showGraphRectangle, listGraphToListRect
from src.ManageFileGXL import readFileGXL, readAllFileGXL
from src.parse import extractRect
from src.video import extractFrame, showRect, showObjectRect, showObjectRectWithGraph

def main():
    # name of video directory
    dirVideo = 'T5/' 
    #dirVideo = 'MOT16-09/'
    #dirVideo = 'PETS09-S2L1/' 
    #dirVideo = 'ADL-Rundle-1/'
    
    
    # root path of project
    rootPath = '../../'
    # path of video
    video = rootPath + 'video/' + dirVideo
    graphPath = rootPath + 'graphGXL/'+ dirVideo
    
    
    listFrame = extractFrame(video + 'img1/')
    listRect = extractRect(video + 'det/det.txt')
    
    
    # for see all rectangle on video
    showRect(listFrame, listRect)
    
    # generate and save all graphs in GXL files
    generateAllGraph(listFrame, listRect, graphPath)
    
    # for see one graph
    #g2 = readFileGXL(rootPath + 'graphGXL/'+ dirVideo +'1.gxl')
    #showGraphRectangle(g2)
    
    
    # read all graphs
    listGraph = readAllFileGXL(graphPath)
    listRectRead = listGraphToListRect(listGraph)
    
    # for see all rectangles on video
    #showObjectRect(listFrame, listRectRead)
    
    # for see all rectangles with associate graph on video
    showObjectRectWithGraph(listFrame, listGraph)

main()

