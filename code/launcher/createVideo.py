from src.rectangle import Rectangle
from videoTest.CreateVideo import createVideo

rootPath = '../../'

# create a test video
def main():
    listRectStart = []
    listRectEnd = []
    listRectStart.append(Rectangle((10,10), (20,30), color=(255,0,0)))
    listRectEnd.append(Rectangle((300,400), (310,420)))
    listRectStart.append(Rectangle((350,40), (360,50), color=(0,255,0)))
    listRectEnd.append(Rectangle((40,40), (50,50)))
    listRectStart.append(Rectangle((350,100), (370, 130), color=(0,0,255)))
    listRectEnd.append(Rectangle((150,450), (190,510)))
    listRectStart.append(Rectangle((450,100), (450, 100), color=(0,128,128)))
    listRectEnd.append(Rectangle((450,300), (470,340)))
    listRectStart.append(Rectangle((0,150), (20, 170), color=(128,128,0)))
    listRectEnd.append(Rectangle((700,250), (730,280)))
    
    
    createVideo(listRectStart, listRectEnd, (400, 600), 100, rootPath + "video/T5/")

main()
