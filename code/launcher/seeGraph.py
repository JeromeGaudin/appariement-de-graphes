#-*- coding: utf-8 -*-
#!/usr/bin/env python

import cv2 

from src.graph import showGraphRectangle
from src.ManageFileGXL import readFileGXL
from src.parse import extractRect
from src.video import drawDetection, extractFrame

def main():
    # name of video directory
    #dirVideo = 'T5/' 
    #dirVideo = 'MOT16-09/'
    dirVideo = 'PETS09-S2L1/' 
    #dirVideo = 'ADL-Rundle-1/'
    
    
    # root path of project
    rootPath = '../../'
    # path of video
    video = rootPath + 'video/' + dirVideo
    graphPath = rootPath + 'graphGXL/'+ dirVideo
    
    
    listFrame = extractFrame(video + 'img1/')
    listRect = extractRect(video + 'det/det.txt')
    
    # frame to see
    indexFrame = 15
    
    #show one frame with detection
    frame = listFrame[indexFrame].copy()
    for j in range(0, len(listRect[indexFrame])):
        drawDetection(frame, listRect[indexFrame][j][0], listRect[indexFrame][j][1], j)
    cv2.imshow('myImage', frame)
    
    # for see one graph
    g = readFileGXL(graphPath + str(indexFrame + 1) + '.gxl')
    showGraphRectangle(g)
    
main()

