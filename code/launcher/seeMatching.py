
from src.graph import listGraphToListRect
from src.ManageFileGXL import readAllFileGXL 
from src.video import extractFrame, showObjectRectWithGraph, showObjectRect


def main():
    # name of video directory
    #dirVideo = 'T5/'
    dirVideo = 'PETS09-S2L1/'
    #dirVideo = 'MOT16-09/'
    
    
    
    rootPath = '../../'
    video = rootPath + 'video/' + dirVideo
    graphPath = rootPath + 'graphGXL/'+ dirVideo
    
    listFrame = extractFrame(video + 'img1/')
    
    # read all graph
    listGraph = readAllFileGXL(graphPath)
    
    # for see all rectangle on video
    #listRectRead = listGraphToListRect(listGraph)
    #showObjectRect(listFrame, listRectRead)
    
    # for see all rectangle with associate graph on video
    showObjectRectWithGraph(listFrame, listGraph)
    
main()

